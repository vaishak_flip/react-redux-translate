import React from "react";
import { withLocalize } from "react-localize-redux";

class LanguageToggle extends React.Component {

    constructor(props) {
        super(props);
        this.langChange = this.langChange.bind(this)
    }
    langChange(event) {
        const { setActiveLanguage } = this.props;
        setActiveLanguage(event.target.value);
    }

    render() {
    const { languages, activeLanguage } = this.props;
    console.log(activeLanguage);
return (
  <select className="selector" onChange={this.langChange}>
    {languages.map(lang => (
      <option key={lang.code} value={lang.code} selected={activeLanguage.code === lang.code}>
          {lang.name}
      </option>
    ))}
  </select>
);
    }
}

export default withLocalize(LanguageToggle);