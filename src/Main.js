import React from "react";
import { renderToStaticMarkup } from "react-dom/server";
import { withLocalize, Translate } from "react-localize-redux";
import globalTranslations from "./translations/global.json";

import LanguageToggle from './LanguageToggle'

class Main extends React.Component {
  constructor(props) {
    super(props);

    const languages = [
      { name: "English", code: "en" },
      { name: "French", code: "fr" },
      { name: "Malayalam", code: "ml" },
    ];
    // const languages = ["en", "fr", "ml"];
    const defaultLanguage =
        (localStorage && localStorage.getItem("userLanguage")) || languages[0].code;

    this.props.initialize({
      languages,
      translation: globalTranslations,
      options: { renderToStaticMarkup, defaultLanguage }
    });
  }

  componentDidUpdate(prevProps) {
    const prevLangCode =
      prevProps.activeLanguage && prevProps.activeLanguage.code;
    const curLangCode =
      this.props.activeLanguage && this.props.activeLanguage.code;

    const hasLanguageChanged = prevLangCode !== curLangCode;

    if (hasLanguageChanged) {
      localStorage.setItem("userLanguage", curLangCode);
    }
  }

  render() {
    return (
      <React.Fragment>
        <LanguageToggle />
        <p>
          <Translate id="greeting" />
        </p>
        <p>
          <Translate id="farewell" />
        </p>
      </React.Fragment>
    )
  }
}

export default withLocalize(Main);