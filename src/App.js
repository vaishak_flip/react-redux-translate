import React, { Component } from 'react';
import './App.css';
import Main from "./Main";
import { BrowserRouter as Router, Route } from "react-router-dom";
import { LocalizeProvider } from "react-localize-redux";

class App extends Component {
  render() {
    return (
      <LocalizeProvider>
        <Router>
          <Route path="/" component={Main} />
        </Router>
      </LocalizeProvider>
    );
  }
}

export default App;
